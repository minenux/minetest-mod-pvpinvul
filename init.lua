

-- Copyright (C) 2021 Sandro Del Toro De Ana
-- Copyright (C) 2022 PICCORO Lenz McKAY

-- This file is part of antimod minenux Minetest Mod.

local kill_hitter = core.settings:get_bool("antipvp_kill_hitter") or false

local admin_privs = core.settings:get_bool("antipvp_admin_privs") or true

local send_notify = core.settings:get_bool("antipvp_send_notify") or true

local send_to_all = core.settings:get_bool("antipvp_send_to_all") or false

local area_ratio = tonumber(minetest.setting_get("antipvp_area_ratio") or 2)

if kill_hitter == nil then kill_hitter = false end
if admin_privs == nil then admin_privs = true end
if send_notify == nil then send_notify = true end
if send_to_all == nil then send_to_all = false end
if area_ratio == nil then area_ratio = 2 end

-- compat with irc mod fork
if core.get_modpath("irc") then
   if irc.saysec == nil then
      irc.saysec = irc.say
   end
end

local privs_give_to_admin = false

if admin_privs then  privs_give_to_admin = true end

-- priv
core.register_privilege("antipvp", {
   description = "Invulnerable in PVP",
   give_to_singleplayer = false,
   give_to_admin = privs_give_to_admin,
})

-- block
core.register_node("antipvp:quiet", {
	description = "Anti PVP place",
	tiles = {"default_obsidian.png^[colorize:green:10^heart.png"},
	paramtype = "light",
	paramtype2 = "wallmounted",
	is_ground_content = false,
	sunlight_propagates = true,
	walkable = false,
	groups = {choppy=2, level=3}
})

-- craft
minetest.register_craft({
	output = "antipvp:quiet",
	recipe = {
            { "default:obsidianbrick", "default:goldblock", "default:obsidianbrick" },
            { "default:goldblock",     "flowers:rose",          "default:goldblock" },
            { "default:obsidianbrick", "default:goldblock", "default:obsidianbrick" },
	}
})

-- action
core.register_on_punchplayer(
   function(player, hitter, time_from_last_punch, tool_capabilities, dir, damage)

      -- only in pvp
      if not hitter:is_player() or not player:is_player() then
	 return
      end
      
      local player_name = player:get_player_name()
      local hitter_name = hitter:get_player_name()
      local player_invul = core.check_player_privs(player_name, {antipvp=true})
      local hitter_invul = core.check_player_privs(hitter_name, {antipvp=true})
      local player_ppos = player:getpos()
      
      -- first we check if player its invulnerable
      if player_invul then
	 if kill_hitter and not hitter_invul then
	    hitter:set_hp(0)
	 end

         if send_notify then
	   core.chat_send_player(hitter_name, "You can not hurt a God" )
	   core.chat_send_player(player_name, hitter_name.." try hurt you" )
	   if core.get_modpath("irc") then
		    if send_to_all then
			irc.saysec(hitter_name.." try hurt to "..player_name)
		    else
			irc.saysec(player_name, hitter_name.." try hurt you")
		    end
	    end
	 end
	 minetest.log("action","[antipvp] "..hitter_name.." try hurt to "..player_name)
	 return true
      end
      
      if not player_ppos then
            return false
      end
      -- not a invulnerable player.. then check if are over protected place
	if core.find_node_near(player_ppos, area_ratio, "antipvp:quiet") then
	    if send_notify then
		core.chat_send_player(hitter_name, "is at a peaceful area... ")
		if core.get_modpath("irc") then
		    if send_to_all then
			irc.saysec(hitter_name.." try hurt on of the peacefull areas")
		    else
			irc.saysec(hitter_name, "Dont try hurt the peacefull area")
		    end
		end
	    end
	    minetest.log("action","[antipvp] "..hitter_name.." try hurt to "..player_name)
	    return true
	end
       
   end
)

-- ANTI GRIEF part

-- prevents placing of lava/liquid sources or buckets above a point of y


--water
--bucket:bucket_water

function prevent_place_above(name)

	local old_on_place=minetest.registered_craftitems[name];--.on_place;
	local old_after_place_node = minetest.registered_nodes[name];--.after_place_node;
	--after_place_node = func(pos, placer, itemstack, pointed_thing)
	
	if old_on_place and old_on_place.on_place then
		old_on_place=old_on_place.on_place;
		minetest.registered_craftitems[name].on_place=function(itemstack, placer, pointed_thing)
			local pos = pointed_thing.above
			if pos.y>0 then
				minetest.log("error","grifeo ANTI GRIEF " .. placer:get_player_name() .. " tried to place " .. name .. " at " .. minetest.pos_to_string(pos));
				return itemstack
			else
				return old_on_place(itemstack, placer, pointed_thing)
			end
		end
	return;
	end

	if old_after_place_node then

		old_after_place_node=old_after_place_node.after_place_node
		
		local table = minetest.registered_nodes[name];
		local table2 = {}
		for i,v in pairs(table) do
			table2[i] = v
		end
		
		table2.after_place_node=function(pos, placer, itemstack, pointed_thing)
			--after_place_node = func(pos, placer, itemstack, pointed_thing)
			local pos = pointed_thing.above
			if pos.y>60 then
				minetest.log("warning","[anti] mod detected a GRIEF " .. placer:get_player_name() .. " tried to place " .. name .. " at " .. minetest.pos_to_string(pos));
				minetest.set_node(pos, {name = "air"});
				return itemstack
			end
--			if pos.x>60 and pos.x < -60 then
--			end
		end
		
		
		minetest.register_node(":"..name, table2)
		return;
	end 
	
		
	return;
end
	

minetest.after(45,
function ()
--prevent_place_above("bucket:bucket_water");
--prevent_place_above("default:water_source");
prevent_place_above("bucket:bucket_lava");
prevent_place_above("default:lava_source");
end
)

print("[MOD] anti loaded")

